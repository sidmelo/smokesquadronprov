﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UISelectionManager : MonoBehaviour {

    public EventSystem eventSystem;
    private GameObject selectedObject;

    // Start is called before the first frame update
    void Start() {
        selectedObject = eventSystem.firstSelectedGameObject;
    }

    // Update is called once per frame
    void Update() {
        if ( eventSystem.currentSelectedGameObject != selectedObject ) {
            if ( eventSystem.currentSelectedGameObject == null ) {
                eventSystem.SetSelectedGameObject(selectedObject);
            } else {
                selectedObject = eventSystem.currentSelectedGameObject;
            }

        }
    }

    public void SetSelectedGameObject(GameObject newSelectedObject) {
        selectedObject = newSelectedObject;
        eventSystem.SetSelectedGameObject(selectedObject);
    }
}
