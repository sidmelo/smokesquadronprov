﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonBombController : MonoBehaviour {

    public GameObject explosionParticle;
    public float bombDamage = 30;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {

        switch ( other.tag ) {
            case "Player01":
                other.gameObject.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)bombDamage);
                break;
            case "Castle":
                other.gameObject.GetComponentInParent<CastleController>().TakeDamage((int)bombDamage);
                break;
            case "Smoke":
                other.gameObject.GetComponent<SmokeController>().TakeDamage(bombDamage);
                break;
            default:
                break;
        }

        GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
        Destroy(explosion, 3f);
        Destroy(gameObject);
    }
}
