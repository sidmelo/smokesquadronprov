﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximitySensor : MonoBehaviour {

    public Transform target;
    public string targetTag;
    public float updatePositionTime = 0.2f;

    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start() {
        _audioSource = GetComponent<AudioSource>();
        InvokeRepeating("UpdatePosition", updatePositionTime, updatePositionTime);
    }

    // Update is called once per frame
    void Update() {
       
    }

    private void UpdatePosition() {
        transform.position = target.position;
    }

    private void OnTriggerEnter(Collider other) {
        
        switch ( other.tag ) {
            case "Smoke":
                SmokeController smoke = other.gameObject.GetComponent<SmokeController>();
                if ( targetTag != smoke.GetParentTag() ) {
                    if ( !_audioSource.isPlaying ) {
                        _audioSource.Play();
                    }
                }
                break;
            case "Wall":
                if ( !_audioSource.isPlaying ) {
                    _audioSource.Play();
                }
                break;
            default:
                break;
        }
    }

    private void OnTriggerExit(Collider other) {
        
        switch ( other.tag ) {
            case "Smoke":
                SmokeController smoke = other.gameObject.GetComponent<SmokeController>();
                if ( targetTag != smoke.GetParentTag() ) {
                    _audioSource.Stop();
                }
                break;
            case "Wall":
                _audioSource.Stop();
                break;
            default:
                break;
        }
    }

    public void StopSensor() {
        _audioSource.Stop();
    }
}
