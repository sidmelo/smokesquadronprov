﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Aeroplane;

public class TrailController : MonoBehaviour {

    [Header("Trails")]
    public TrailRenderer[] trails;
    public Material defaultMaterial;
    public Material fastMaterial;

    [Header("Smoke")]
    public ParticleSystem smokeParticles;

    private bool _isAccelerating = false;
    private AeroplaneCustomInput _input;

    // Use this for initialization
    void Start () {
        _input = GetComponent<AeroplaneCustomInput>();

        var main = smokeParticles.main;
        if ( gameObject.CompareTag("Player01") ) {
            main.startColor = GameController.Instance.Team01Color;
        }
        if ( gameObject.CompareTag("Player02") ) {
            main.startColor = GameController.Instance.Team02Color;
        }
    }
	
	// Update is called once per frame
	void Update () {
        SetSpeedState(_input.IsAccelerating());
    }

    public void SetSpeedState(bool isAcceleraring) {
        if( isAcceleraring != _isAccelerating ) {
            _isAccelerating = isAcceleraring;
            if ( _isAccelerating )
                SetFastMaterial();
            else
                SetDefaultMaterial();
        }
    }

    void SetFastMaterial() {
        foreach ( TrailRenderer trail in trails ) {
            trail.Clear();
            trail.material = fastMaterial;
        }
    }

    void SetDefaultMaterial() {
        foreach ( TrailRenderer trail in trails ) {
            trail.material = defaultMaterial;
        }
    }


}
