﻿using UnityEngine.AI;
using UnityEngine;

public class TankController : MonoBehaviour {

    public NavMeshAgent agent;
    public Transform destination;
    public GameObject shootParticle;
    public Transform cannonPosition;


    public AudioClip hitSound;
    public float destDistRadius = 300f;
    public float shootingRate;
    public float shotDamage;
    public float MaxHealth;

    private AudioSource audioSource;
    private bool _isNotShooting = true;
    private float _currentHealth;
    private bool _isDead = false;

	// Use this for initialization
	void Start () {
        _currentHealth = MaxHealth;
        agent.SetDestination(GetPositionAroundObject(destination));

        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (_isNotShooting) {
            // Check if we've reached the destination
            if ( !agent.pathPending ) {
                if ( agent.remainingDistance <= agent.stoppingDistance ) {
                    if ( !agent.hasPath || agent.velocity.sqrMagnitude == 0f ) {
                        // Done
                        _isNotShooting = false;
                        InvokeRepeating("ShootCannon", 0f, shootingRate);                        
                    }
                }
            }             
        }
	}

    private void ShootCannon() {
        transform.LookAt(destination);
        GameObject explosion = Instantiate(shootParticle, cannonPosition.position, Quaternion.identity);
        Destroy(explosion, 3f);
        RaycastHit _hit;
        if ( Physics.Raycast(cannonPosition.position, cannonPosition.forward, out _hit, 500f) ) {
            //Physics.Raycast(camera.transform.position, camera.transform.forward, out _hit, machineGun.range, mask)
            //print(_hit.collider.name);
                        
            GameObject hitEffect = Instantiate(shootParticle, _hit.point, Quaternion.LookRotation(_hit.normal));
            Destroy(hitEffect, 3f);

            switch ( _hit.collider.tag ) {
                case "Player01":
                    _hit.collider.gameObject.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)shotDamage);
                    break;
                case "Castle":
                    _hit.collider.gameObject.GetComponentInParent<CastleController>().TakeDamage((int)shotDamage);
                    break;
                case "Smoke":
                    _hit.collider.gameObject.GetComponent<SmokeController>().TakeDamage(shotDamage);
                    break;
                default:
                    break;
            }
        }
    }

    public void TakeDamage(float dmg) {
        _currentHealth -= dmg;
        audioSource.PlayOneShot(hitSound);
        if (_currentHealth <= 0f && !_isDead) {
            _isDead = true;
            EnemyWavesController.Instance.EnemyDestroyed();
            GameObject explosion = Instantiate(shootParticle, transform.position, Quaternion.identity);
            Destroy(explosion, 3f);
            Destroy(gameObject);
        }
    }

    Vector3 GetPositionAroundObject(Transform tx) {
        Vector3 offset = Random.insideUnitCircle * destDistRadius;
        Vector3 pos = tx.position + new Vector3 (offset.x, 0f, offset.y);
        return pos;
    }
}
