﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITrackGameObject : MonoBehaviour {

    public GameObject TrackObject;
    public Camera renderCamera;

    Image _image;

    private void Start() {
        _image = GetComponent<Image>();
    }


    void Update() {

        Vector3 pos = renderCamera.WorldToScreenPoint(TrackObject.transform.position);
        Vector3 vpos = renderCamera.WorldToViewportPoint(TrackObject.transform.position);
        pos.z = 0f;

        transform.position = pos;

        if (vpos.x > 0f && vpos.x < 1f && vpos.z > 0) {
            _image.enabled = true;
        } else {
            _image.enabled = false;
        }

        //print(vpos);

    }
}


