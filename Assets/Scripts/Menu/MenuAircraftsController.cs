﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AirplaneType {
    LEIR7,
    OISSAK
}

[System.Serializable]
public struct SelectableAirplane {
    public AirplaneType type;
    public GameObject airplaneObject;
}

public class MenuAircraftsController : MonoBehaviour {
    //Script para alternar os aviões de escolha dos jogadores

    //public GameObject airplaneSelector;
    public GameObject airplaneSelectIcon;
    public GameObject airplaneSelectText;
    public GameObject airplaneConfirmationText;
    public SelectableAirplane[] airplaneObjects;

    public Sprite setAirplane;
    public Sprite setController;

    public int playerNumber;

    private Dictionary<AirplaneType, GameObject> airplanesDictionary;
    private int _inputNumber = -1;
    private AirplaneType _currentAirplane;
    private bool _playerSelectorEnabled = false, _changeEnabled = false, _airplaneSelected = false;
    private Animator _animator;

    public bool AirplaneSelected{
        get {
            return _airplaneSelected;
        }
    }

    public bool PlayerSelectorEnabled     {
        get {
            return _playerSelectorEnabled;
        }
    }

    public int InputNumber{
        get {
            return _inputNumber;
        }
    }

    void Start() {
        //airplaneSelector.SetActive(false);
        _animator = GetComponent<Animator>();        
        DeactivateStartAirplanes();
    }
    
    //instantiates the dictionary if its not already
    private void StartDictionary() {
        if ( airplanesDictionary == null) {
            airplanesDictionary = new Dictionary<AirplaneType, GameObject>();
            _currentAirplane = AirplaneType.LEIR7;
            foreach ( SelectableAirplane sAirplane in airplaneObjects ) {
                airplanesDictionary.Add(sAirplane.type, sAirplane.airplaneObject);
            }
        }
        
    }

    //verifies the game mode and enables the selection
    public void StartAirplaneSelection(GameMode modeGame) {
        DeactivateStartAirplanes();
        switch ( modeGame ) {
            case GameMode.m1x1:
                if ( playerNumber <= 1) {
                    _playerSelectorEnabled = true;
                    StartAirplaneSelector();
                }
                break;
            default:
                _playerSelectorEnabled = true;
                StartAirplaneSelector();
                break;
        }

        
        ////Desativa todos os avioes inicialmente
        //airplane1.SetActive(false);
        //airplane2.SetActive(false);
        //airplane3.SetActive(false);
        //airplane4.SetActive(false);
        //DeactivateStartAirplanes(airplanes1);
        //DeactivateStartAirplanes(airplanes2);
        //DeactivateStartAirplanes(airplanes3);
        //DeactivateStartAirplanes(airplanes4);

        //// 0 = 1x1 | 1 = 1x1x1x1 | 2 = 2x2
        ////Depois ativa só o primeiro
        //if ( modeGame == GameMode.m1x1 ) {
        //    // Tem pelo menos 2 controles para o modo 1x1
        //    if ( InputHandler.Instance.GetNumControllers() >= 2 ) {
        //        airplaneSelect2.GetComponent<SpriteRenderer>().sprite = setAirplane;
        //        airplanes1[0].SetActive(true);
        //        airplane1.SetActive(true);
        //        airplanes2[0].SetActive(true);
        //        airplane2.SetActive(true);
        //        airplaneSelectText2.SetActive(false);
        //    } else {
        //        airplaneSelect2.GetComponent<SpriteRenderer>().sprite = setController;
        //        airplanes1[0].SetActive(true); airplane1.SetActive(true);
        //        airplane2.SetActive(true); airplaneSelectText2.SetActive(true);
        //    }

        //} else if ( modeGame == GameMode.mx4 || modeGame == GameMode.m2x2) {
        //    if ( InputHandler.Instance.GetNumControllers() == 4 ) {
        //        airplaneSelect2.GetComponent<SpriteRenderer>().sprite = setAirplane;
        //        airplaneSelect3.GetComponent<SpriteRenderer>().sprite = setAirplane;
        //        airplaneSelect4.GetComponent<SpriteRenderer>().sprite = setAirplane;
        //        airplanes1[0].SetActive(true); airplane1.SetActive(true);
        //        airplanes2[0].SetActive(true); airplane2.SetActive(true); airplaneSelectText2.SetActive(false);
        //        airplanes3[0].SetActive(true); airplane4.SetActive(true); airplaneSelectText3.SetActive(false);
        //        airplanes3[0].SetActive(true); airplane4.SetActive(true); airplaneSelectText4.SetActive(false);
        //    } else {
        //        airplaneSelect2.GetComponent<SpriteRenderer>().sprite = setController;
        //        airplaneSelect3.GetComponent<SpriteRenderer>().sprite = setController;
        //        airplaneSelect4.GetComponent<SpriteRenderer>().sprite = setController;
        //        airplanes1[0].SetActive(true); airplane1.SetActive(true);
        //        airplane2.SetActive(true); airplaneSelectText2.SetActive(true);
        //        airplane3.SetActive(true); airplaneSelectText3.SetActive(true);
        //        airplane4.SetActive(true); airplaneSelectText4.SetActive(true);
        //    }
        //}
        
    }

    //changes the last model for the next one
    public void ChangeAirplaneObject() {
        for ( int i = 0; i < airplanesDictionary.Count; i++ ) {
            airplanesDictionary[(AirplaneType)i].SetActive(false);
        }
        airplanesDictionary[_currentAirplane].SetActive(true);
    }
    
    //re-enables the airplane change
    public void EnableChange() {
        _changeEnabled = true;
    }

    // if this player is enabled on current game mode start selectior and wait for player to enter
    private void StartAirplaneSelector() {
        airplaneSelectIcon.GetComponent<SpriteRenderer>().sprite = setController;
        airplaneSelectIcon.SetActive(true);
        airplaneSelectText.SetActive(true);
    }

    //when player enters, activate the airplane selector
    private void ActivateAirplaneSelector() {
        airplaneSelectIcon.GetComponent<SpriteRenderer>().sprite = setAirplane;        
        airplanesDictionary[_currentAirplane].SetActive(true);
        airplaneSelectIcon.SetActive(true);
        airplaneSelectText.SetActive(false);
        _animator.enabled = true;
    }

    void DeactivateStartAirplanes() {
        //disables icon if not int the game mode
        if(! _playerSelectorEnabled )
            airplaneSelectIcon.SetActive(false);

        StartDictionary();
        if ( airplanesDictionary.Count > 0 ) {
            for ( int i = 0; i < airplanesDictionary.Count; i++ ) {
                airplanesDictionary[(AirplaneType)i].SetActive(false);
            }
        }
    }

    // changes _currentAirplane based on player input
    private void ChangeAirplane(float selection) {

        if (selection > 0) {
            if ( ((int)_currentAirplane) + 1 < airplanesDictionary.Count ) {
                _currentAirplane++;
            } else {
                _currentAirplane = (AirplaneType)0;
            }
        }

        if ( selection < 0 ) {
            if ( ((int)_currentAirplane) - 1 < 0 ) {
                _currentAirplane = (AirplaneType)(airplanesDictionary.Count - 1);                
            } else {
                _currentAirplane--;
            }
        }

        _changeEnabled = false;
        _animator.SetTrigger("Out");
    }

    // Update is called once per frame
    void Update() {
        if ( !_playerSelectorEnabled ) return;

        //sets the _inputNumber when defined player enters
        if ( _inputNumber == -1 && InputHandler.Instance.GetNumControllers() > playerNumber ) {
            _inputNumber = InputHandler.Instance.GetPlayerInputNumber(playerNumber);
            ActivateAirplaneSelector();
        }

        if ( !_airplaneSelected ) {

            //change the selected airplane based on player input, only when able
            if ( _changeEnabled ) {
                float selection = Input.GetAxis(InputHandler.Instance.PitchAxis + _inputNumber);                
                if ( selection != 0 )
                    ChangeAirplane(selection);

                //select current airplane
                if ( Input.GetButtonDown(InputHandler.Instance.MachineGun + _inputNumber) ) {
                    _airplaneSelected = true;
                    airplaneConfirmationText.SetActive(true);
                    airplaneSelectIcon.SetActive(false);
                }
            }

            //back to main menu if player 01
            //if ( _inputNumber != -1 && playerNumber == 0 && Input.GetButtonDown(InputHandler.Instance.SW01 + _inputNumber) ) {
            //    ResetsSelection();
            //    MainMenuController.Instance.HideMenuSelection();
            //}

        } else {
            //re-enables the selection if the player changes his mind
            if ( Input.GetButtonDown(InputHandler.Instance.SW01 + _inputNumber) ) {
                _airplaneSelected = false;
                airplaneConfirmationText.SetActive(false);
                airplaneSelectIcon.SetActive(true);
            }

            ////starts the game if player 01
            //if (playerNumber == 0 && Input.GetButtonDown(InputHandler.Instance.MachineGun + _inputNumber) ) {
            //    MainMenuController.Instance.PlayGame();
            //}

        }
    }

    public void ResetsSelection() {
        _inputNumber = -1;
        _airplaneSelected = false;
        _playerSelectorEnabled = false;
        _changeEnabled = false;
        _animator.enabled = false;
        airplaneConfirmationText.SetActive(false);
        //airplaneSelectIcon.SetActive(true);
    }



}
