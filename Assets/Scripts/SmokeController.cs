﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeController : MonoBehaviour {

    public float health;
    public float colTimer;
    public float lifeSpan;
    public float dissolveTime = 3f;
    public ParticleSystem particles;
    
    //public Color Team03Color;
    //public Color Team04Color;

    private float _curHealth;
    private SphereCollider _collider;
    private string _parentTag = "";
    private MeshRenderer _mesh;


	// Use this for initialization
	void Start () {
        _curHealth = health;
        _collider = GetComponent<SphereCollider>();
        _mesh = GetComponent<MeshRenderer>();
        //particles = GetComponentInChildren<ParticleSystem>();
        Invoke("ActivateCollider", colTimer);
        Invoke("Destroy", lifeSpan);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TakeDamage(float damage) {
        _curHealth -= damage;
        if (_curHealth <= 0) {
            Destroy(this.gameObject);
        }
    }

    void ActivateCollider() {
        _collider.enabled = true;
    }

    public void SetParentTag(string tag) {
        _parentTag = tag;
        var main = particles.main;
        switch ( tag ) {
            case "Player01":                
                main.startColor = GameController.Instance.Team01Color;
            break;
            case "Player02":
                main.startColor = GameController.Instance.Team02Color;
                break;
            default:
                break;
        }
    }

    public string GetParentTag() {
        return _parentTag;
    }

    private void Destroy() {
        _mesh.enabled = false;
        _collider.enabled = false;
        var main = particles.main;
        main.loop = false;
        Destroy(this.gameObject, dissolveTime);
    }
}
