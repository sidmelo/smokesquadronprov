﻿using UnityEngine;

[System.Serializable]
public class Weapon {

    public string name = "new weapon";
    public float damage = 10f;
    public float range = 100f;
    public int fireRate = 10;
    public float speed = 10f;
    public int maxLoad = 20;
}
