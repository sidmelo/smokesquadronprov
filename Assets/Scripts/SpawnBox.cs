﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBox : MonoBehaviour {

    public Transform maxPoint;
    public Transform minPoint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Vector3 GetRandomPosition() {

        return new Vector3(Random.Range(minPoint.position.x, maxPoint.position.x), Random.Range(minPoint.position.y, maxPoint.position.y), Random.Range(minPoint.position.z, maxPoint.position.z));
                
    }
}
