﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public enum GameMode { // 0 = 1x1 | 1 = 1x1x1x1 | 2 = 2x2
    m1x1,
    mx4,
    m2x2
}

public class MainMenuController : MonoBehaviour {

    public PlayerData PlayerData { get; private set; }
        
    public AudioMixer audioMixerGeneral;

    //Tela de seleção de aviões
    [Header("UI References")]
    public UISelectionManager uiSelectionManager;
    public Slider sliderVolume;
    public Dropdown dropdownGraphics;
    public Button dualButton;
    public Button fourButton;
    public Button teamButton;
    public GameObject mainMenuPlayButton;
    public GameObject selectionStartButton;

    [Header("Load Screen")]
    public GameObject loadScreen;
    public Sprite[] loadScreensImage;

    //Tela de seleção de aviões
    [Header("Airplane Selection")]
    public GameObject selectionMenuScreen;
    public GameObject playMenuScreen;
    public MenuAircraftsController[] selectionControllers;

    private bool _allSelected = false;
    private GameMode _currentGameMode;

    public static MainMenuController Instance;

    private void Start() {

        if (Instance == null) {
            Instance = this;
        }

        PlayerData = PlayerDataPersistence.loadData();
        // Atualiza as preferencias
        // Altera os valores do jogo de acordo com preferencias e altera no visual também
        audioMixerGeneral.SetFloat("volumeGeneral", PlayerData.volumeGeneral);
        sliderVolume.value = PlayerData.volumeGeneral;
        QualitySettings.SetQualityLevel(PlayerData.qualityGeneral);
        dropdownGraphics.value = PlayerData.qualityGeneral;

        //InputHandler.Instance.AssignPlayerInput();
        //dualButton.interactable = false;

        //InvokeRepeating("EnableMultiplayer", 0f, 2f);
    }

    public void ShowMenuSelection(int modeGame) {
        // 0 = 1x1 | 1 = 1x1x1x1 | 2 = 2x2
        playMenuScreen.SetActive(false);
        InputHandler.Instance.AssignPlayerInput();
        foreach ( MenuAircraftsController selector in selectionControllers ) {
            selector.StartAirplaneSelection((GameMode)modeGame);
        }
        _currentGameMode = (GameMode)modeGame;
        selectionMenuScreen.SetActive(true);
    }

    public void HideMenuSelection() {
        foreach ( MenuAircraftsController selector in selectionControllers ) {
            selector.ResetsSelection();
        }
        selectionMenuScreen.SetActive(false);
        playMenuScreen.SetActive(true);
        uiSelectionManager.SetSelectedGameObject(mainMenuPlayButton);
    }

    public void PlayGame() {

        if (_allSelected) {
            switch ( _currentGameMode ) {
                case GameMode.m1x1:
                    CallLoadScreen();
                    SceneManager.LoadScene("Battle_1v1");
                    break;
                case GameMode.mx4:
                    //CallLoadScreen();
                    //SceneManager.LoadScene("PlaygroundTutorial");
                    print("not available yet");
                    break;
                case GameMode.m2x2:
                    CallLoadScreen();
                    SceneManager.LoadScene("Battle_2v2");
                    break;
                default:
                    break;
            }
        }
    }

    public void PlayGameSingle() {
        CallLoadScreen();
        SceneManager.LoadScene("PlaygroundTutorial");
    }

    public void PlayGame1x1() {
        CallLoadScreen();
        SceneManager.LoadScene("Battle_1v1");
    }

    public void PlayGame2x2() {
        CallLoadScreen();
        SceneManager.LoadScene("Battle_2v2");
    }

    public void QuitGame() {
        Debug.Log("quit");
        Application.Quit();
    }

    public void SetVolume(float volume) {
        audioMixerGeneral.SetFloat("volumeGeneral", volume);
        PlayerDataPersistence.SaveData("float", "volumeGeneral", volume.ToString());
    }

    public void SetQuality(int qualityIndex) {
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerDataPersistence.SaveData("int", "qualityGeneral", qualityIndex.ToString());
    }

    private void EnableMultiplayer() {
        if ( InputHandler.Instance.GetNumControllers() > 1 ) {
            dualButton.interactable = true;
        }
        if ( InputHandler.Instance.GetNumControllers() > 3 ) {
            fourButton.interactable = true;
            teamButton.interactable = true;
        }
    }

    private void CallLoadScreen() {
        int imageSelected = Random.Range(0, loadScreensImage.Length);
        if ( loadScreensImage[imageSelected] ) {
            loadScreen.GetComponent<Image>().sprite = loadScreensImage[imageSelected];
            loadScreen.SetActive(true);
        }
    }

    private void Update() {

        if ( selectionMenuScreen.activeInHierarchy) {
            _allSelected = true;
            foreach ( MenuAircraftsController selector in selectionControllers ) {
                if ( selector.PlayerSelectorEnabled && !selector.AirplaneSelected ) {
                    _allSelected = false;

                    if ( selector.InputNumber == 1) {
                        if ( Input.GetButtonDown(InputHandler.Instance.SW01 + "1") ) {
                            print("voltou do menu");
                            HideMenuSelection();
                        }
                    }
                }
                
            }
            if ( _allSelected ) {
                selectionStartButton.SetActive(true);

                if ( Input.GetButtonDown(InputHandler.Instance.MachineGun + "1") ) {
                    print("foi do menu");
                    PlayGame();
                }
                

                //uiSelectionManager.SetSelectedGameObject(selectionStartButton);
            } else {
                selectionStartButton.SetActive(false);
                
                if (InputHandler.Instance.GetNumControllers() == 0 ) {
                    if ( Input.GetButtonDown(InputHandler.Instance.SW01 + "1") ) {
                        print("voltou do menu");
                        HideMenuSelection();
                    }
                }
            }

            
        }
    }
}
