﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CastleController : MonoBehaviour {


    public float maxHealth;

    private float _currentHealth;

    public GameObject castleObject;
    public GameObject[] towerObjects;
    public GameObject[] wallObjects;

    public GameObject brokenCastleObject;
    public GameObject[] brokenTowerObjects;
    public GameObject[] brokenWallObjects;

    public Image  lifeBar;

    private bool _firstForm = false, _secondForm = false;

    // Use this for initialization
    void Start () {
        _currentHealth = maxHealth;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void FirstForm() {
        for ( int i = 0; i < wallObjects.Length/2; i++ ) {
            wallObjects[i].SetActive(false);
            brokenWallObjects[i].SetActive(true);
        }
        for ( int i = 0; i < towerObjects.Length / 2; i++ ) {
            towerObjects[i].SetActive(false);
            brokenTowerObjects[i].SetActive(true);
        }
    }

    private void SecondForm() {
        for ( int i = wallObjects.Length - 1; i > wallObjects.Length / 2 - 1; i-- ) {
            wallObjects[i].SetActive(false);
            brokenWallObjects[i].SetActive(true);
        }
        for ( int i = towerObjects.Length - 1; i > towerObjects.Length / 2 - 1; i-- ) {
            towerObjects[i].SetActive(false);
            brokenTowerObjects[i].SetActive(true);
        }
        castleObject.SetActive(false);
        brokenCastleObject.SetActive(true);
    }

    public void TakeDamage(float dmg) {
        _currentHealth -= dmg;

        UpdateLifeUI();

        if ( _currentHealth / maxHealth <= 0.64f ) {
            if (!_firstForm) {
                _firstForm = true;
                FirstForm();
            }
        }

        if ( _currentHealth/maxHealth <= 0.34f ) {
            if ( !_secondForm ) {
                _secondForm = true;
                SecondForm();
            }
        }
        
        if ( _currentHealth <= 0f ) {
            //GameObject explosion = Instantiate(shootParticle, transform.position, Quaternion.identity);
            //Destroy(explosion, 3f);
            //Destroy(gameObject);
            GameController.Instance.ShowGameOverUI("Player01");
        }
    }

    void UpdateLifeUI() {
        if ( _currentHealth >= 0 )
            //lifeBar.localScale = new Vector3((float)_currentHealth / (float)maxHealth, 1f, 1f);
            lifeBar.fillAmount = (float)_currentHealth / (float)maxHealth;        
    }
}
