﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyWavesController : MonoBehaviour {

    public static EnemyWavesController Instance;

    public enum EnemyWavesState {
        SPAWNING,
        WAITING,
        FINISHED
    };

    public EnemyWave[] waves;
    public Transform[] tankSpawnPoints;
    public Transform[] ballonSpawnPoints;
    public GameObject tankPrefab;
    public GameObject eliteTankPrefab;
    public GameObject balloonPrefab;
    public GameObject castleObject;
    public GameObject balloonDestObject;
    public float timeBetweenWaves = 5f;
    public Text wavesText;

    private float _timer = 0f;
    private int _waveCount = 0;
    private int _enemyCount = 0;
    private int _enemiesDestroyed = 0;
    private EnemyWavesState _currentWavesState = EnemyWavesState.WAITING;

    // Use this for initialization
    void Awake () {
        Instance = this;
    }
	
	// Update is called once per frame
	void Update () {
        _timer += Time.deltaTime;
        switch ( _currentWavesState ) {
            case EnemyWavesState.SPAWNING:
                SpawnWave();
                break;
            case EnemyWavesState.WAITING:
                wavesText.gameObject.SetActive(true);
                wavesText.text = "Wave " + (_waveCount + 1).ToString();
                if ( _waveCount == 0 ) wavesText.text += "\nDefend the Castle!";
                if (_timer >= timeBetweenWaves) {
                    _timer = 0f;
                    _currentWavesState = EnemyWavesState.SPAWNING;

                    wavesText.gameObject.SetActive(false);
                }
                break;
            case EnemyWavesState.FINISHED:
                break;
            default:
                break;
        }
    }

    private void SpawnWave() {
        if ( _enemyCount < waves[_waveCount].enemies.Length ) {
            EnemySpawn enemy = waves[_waveCount].enemies[_enemyCount];
            if ( _timer >= enemy.time ) {
                switch ( enemy.enemyType ) {
                    case EnemyType.BALLOON:
                        GameObject ballon = Instantiate(balloonPrefab, ballonSpawnPoints[Random.Range(0, ballonSpawnPoints.Length)].position, balloonPrefab.transform.rotation);
                        ballon.GetComponent<BalloonController>().destination = balloonDestObject.transform;
                        break;
                    case EnemyType.TANK:
                        GameObject tank = Instantiate(tankPrefab, tankSpawnPoints[Random.Range(0, tankSpawnPoints.Length)].position, tankPrefab.transform.rotation);
                        tank.GetComponent<TankController>().destination = castleObject.transform;
                        break;
                    case EnemyType.ELITE_TANK:
                        GameObject eTank = Instantiate(eliteTankPrefab, tankSpawnPoints[Random.Range(0, tankSpawnPoints.Length)].position, eliteTankPrefab.transform.rotation);
                        eTank.GetComponent<TankController>().destination = castleObject.transform;
                        break;
                    default:
                        break;
                }
                _enemyCount++;                
                //print("wave count " + _waveCount + "/n Enemy Count" + _enemyCount + "/n timer" + _timer);
            }
        } else {
            if ( _enemyCount == _enemiesDestroyed ) {
                _timer = 0f;
                _enemyCount = 0;
                _enemiesDestroyed = 0;
                _waveCount++;
                if ( _waveCount < waves.Length ) {
                    _currentWavesState = EnemyWavesState.WAITING;
                } else {
                    _currentWavesState = EnemyWavesState.FINISHED;
                    GameController.Instance.ShowGameOverUI("");
                }
            }

        }

        
    }

    public void EnemyDestroyed() {
        _enemiesDestroyed++;
    }
}
