﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData {
    // Configurations
    public float volumeGeneral { get; set; } // Volume geral do jogo
    public int qualityGeneral { get; set; } // Qualidade gráfica do jogo
    // Games
    public int gamesPlayed { get; set; } // Qtd de partidas jogadas
    public int kills { get; set; } // Quantidade de kills
    public int deaths { get; set; } // Quantidade de mortes
}


