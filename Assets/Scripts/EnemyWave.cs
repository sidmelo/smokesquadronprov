﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType {
    BALLOON,
    TANK,
    ELITE_TANK
}

[System.Serializable]
public struct EnemySpawn {
    public EnemyType enemyType;
    public float time;
}

[CreateAssetMenu(fileName = "New EnemyWave", menuName = "Enemy Wave")]
public class EnemyWave : ScriptableObject {
    public EnemySpawn[] enemies;
}

