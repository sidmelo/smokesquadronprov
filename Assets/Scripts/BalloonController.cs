﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonController : MonoBehaviour {

    public enum BalloonState {
        LIFTING,
        MOVING,
        SHOOTING
    }

    public float speed;
    public float maxHight;
    public float minDestDist;
    public float shootRate;
    public float maxHealth;
    public Transform destination;
    public GameObject bombPrefab;
    public Vector3 bombDropOffset;
    public GameObject explosionParticle;
    public AudioClip hitSound;

    private AudioSource audioSource;
    private BalloonState _currentState = BalloonState.LIFTING;
    private bool _isNotShooting = true;
    private float _currentHealth;
    private bool _isDead = false;

    // Use this for initialization
    void Start () {
        _currentHealth = maxHealth;

        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {

        switch ( _currentState ) {
            case BalloonState.LIFTING:
                transform.position += transform.forward * speed * Time.deltaTime;
                if ( transform.position.y >= maxHight ) {
                    _currentState = BalloonState.MOVING;
                }
                break;
            case BalloonState.MOVING:
                transform.position = Vector3.MoveTowards(transform.position, destination.position, speed * Time.deltaTime);
                if ( Vector3.Distance(transform.position, destination.position) <= minDestDist )
                    _currentState = BalloonState.SHOOTING;
                break;
            case BalloonState.SHOOTING:
                if ( _isNotShooting ) {                    
                    _isNotShooting = false;
                    InvokeRepeating("DeployBomb", 0f, shootRate);                           
                }
                break;
            default:
                break;
        }
	}

    private void DeployBomb() {
        GameObject bomb = Instantiate(bombPrefab, transform.position + bombDropOffset, Quaternion.identity);
    }

    public void TakeDamage(float dmg) {
        _currentHealth -= dmg;
        audioSource.PlayOneShot(hitSound);
        if ( _currentHealth <= 0f && !_isDead) {
            _isDead = true;
            EnemyWavesController.Instance.EnemyDestroyed();
            GameObject explosion = Instantiate(explosionParticle, transform.position, Quaternion.identity);
            Destroy(explosion, 3f);
            Destroy(gameObject);
        }
    }
}
