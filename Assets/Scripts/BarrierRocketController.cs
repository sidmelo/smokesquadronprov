﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierRocketController : RocketController {

    public GameObject barrier;
    public float activationTime;

    private float _timer = 0f;
    private bool _activated = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!_activated) {
            _timer += Time.deltaTime;
            if ( _timer > activationTime ) {
                _activated = true;
                barrier.SetActive(true);
            }
        }        
	}
}
