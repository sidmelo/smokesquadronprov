﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Aeroplane;

public class AeroplaneWeaponController : MonoBehaviour
{

    public enum RocketType
    {
        SIMPLE,
        BARRIER,
        SMOKE,
        MINE,
        FIREWORKS
    }


    //[Header("Input Settings")]
    // public string machineGunInput = "FireTrigger1";
    //public string rocketInput = "RocketTrigger1";
    //public string throttleInput = "Throttle1";
    //public string smokeInput = "Smoke1";
    //public string rearViewInput = "Rear1";

    [HideInInspector] public bool machineGunDown = false;
    [HideInInspector] public bool machineGunUp = false;
    [HideInInspector] public bool rocketDown = false;
    //[HideInInspector] public bool smokeDown = false;
    //[HideInInspector] public bool smokeUp = false;
    [HideInInspector] public bool rearViewDown = false;
    [HideInInspector] public bool rearViewUp = false;
    [HideInInspector] public bool specialWeapon01Down = false;
    [HideInInspector] public bool specialWeapon02Down = false;
    //[HideInInspector] public bool selectedSW01 = false;
    //[HideInInspector] public bool selectedSW02 = false;
    //[HideInInspector] public bool selectedSW03 = false;
    //[HideInInspector] public bool selectedSW04 = false;

    [Header("Machinegun Configurations")]
    public Weapon machineGun;
    public int machineGunCooldownSpeed;
    public float machineGunCastRadious;
    //public Weapon rocketLauncher;

    [Header("Rockets Configurations")]
    public float simpleRocketMaxLoad;
    public GameObject simpleRocketPrefab;
    public float simpleRocketCooldown;
    public float simpleRocketLoadCooldown;
    public GameObject rocketLauncherObject;
    //public float smokeRocketCooldown;
    public float smokeRocketCost;
    //public Weapon tripleRocket;
    //public GameObject tripleRocketPrefab;
    //public GameObject[] tripleRocketObjects;
    //public Weapon smokeRocketLauncher;
    //public GameObject smokeRocketPrefab;
    public GameObject specialWeapon01Prefab;
    public float specialWeapon01Cooldown;
    public GameObject specialWeapon02Prefab;
    public float specialWeapon02Cooldown;
    [SerializeField] private LayerMask mask;

    //[Header("Special Weapons Configurations")]
    
    
    //public GameObject specialWeapon03Prefab;
    //public float specialWeapon03Cooldown;
    //public GameObject specialWeapon04Prefab;
    //public float specialWeapon04Cooldown;

    [Header("Smoke Configurations")]
    public GameObject smokeLauncherObject;
    public GameObject smokeObject;
    //public int smokeCount = 0;
    public int smokeDeployRate;
    public int smokeCapacity;

    [Header("Player Info")]
    public int maxHealth = 200;
    public int numLifes = 3;
    private int _currentHealth;
    private int _currentLifes;
    public float invulnerabilityTime;
    public float blinkRate;
    private bool _blinkOn = false;

    [Header("Particles")]
    public GameObject hitEffectPrefab;
    public ParticleSystem muzzleFlash;
    public GameObject explosionEffect;
    public LineRenderer bulletLine;

    [Header("UI")]
    //public Image rocketUIImage;
    //public Sprite simpleRocketImage;
    //public Sprite tripleRocketImage;
    public Text bulletsText;
    public Text rocketText;
    public Image smokeBar;
    public Image lifeBar;
    public GameObject[] lifeIcons;

    [Header("Misc")]
    [SerializeField] private Camera camera;
    public GameObject rearCameraPivot;
    public GameObject pilot;
    public GameObject colliders;
    public GameObject fuselage;
    public SpawnBox spawnBox;
    public int wallDamage = 15;


    private float _sw01CooldownCounter = 0, _sw02CooldownCounter = 0; //_sw03CooldownCounter = 0, _sw04CooldownCounter = 0;
    private float _sRocketCooldownCounter = 0;
    private bool _sRocketReady = true, _sw01Ready = true, _sw02Ready = true;//, _sw03Ready = true, _sw04Ready = true;
    private bool _smokeRocketReady = true;
    private bool _isFiringMachineGun = false, _isMachineGunOverHeated;
    private int _smokeCount = 0;
    private float _smokeCapacityCounter = 0;
    //private List<GameObject> _smokeList;
    private float _mGCounter, _rocketCounter = 0f;
    //private float _mGCooldownCounter = 0, _rocketCooldownCounter = 0;
    private float _rocketTimer = 0, _currentRocketLoad = 0;
    private bool _canFireRocket = true;
    private RocketType _currentRocket = RocketType.SIMPLE;
    private Vector3 _startPosition;
    private Quaternion _startRotation;
    //private Rigidbody _rigidbody;
    private bool _enabled = true;
    private AeroplaneAudio _aeroplaneAudio;
    private AeroplaneController _aeroplaneController;
    private AeroplaneCustomInput _aeroplaneInput;
    private bool _collided = false;

    // Use this for initialization
    void Start() {

        _aeroplaneAudio = GetComponent<AeroplaneAudio>();
        _aeroplaneController = GetComponent<AeroplaneController>();
        _aeroplaneInput = GetComponent<AeroplaneCustomInput>();

        /*/smoke pool/*/
        //_smokeList = new List<GameObject>();
        //for ( int i = 0; i < smokeCount; i++ ) {
        //    GameObject newSmoke = Instantiate(smokeObject, transform.position, transform.rotation);
        //    newSmoke.SetActive(false);
        //    _smokeList.Add(newSmoke);
        //}
        /*/smoke pool/*/

        _smokeCapacityCounter = smokeCapacity;
        _currentHealth = maxHealth;
        _currentLifes = numLifes;

        //_startPosition = transform.position;
        //_startRotation = transform.rotation;
        transform.position = spawnBox.GetRandomPosition();
        _currentRocketLoad = simpleRocketMaxLoad;

        UpdateBulletsUI(false);
        UpdateRocketsUI();
        UpdateLifeUI();

        InvokeRepeating("ReleaseSmoke", 5f, 1f / smokeDeployRate);

    }

    // Update is called once per frame
    public void HandleWeapons() {
        if ( _enabled ) {
            //if ( Input.GetAxis(machineGunInput) < 0f && !_isFiringMachineGun ) {
            //    if ( !_isMachineGunOverHeated ) {
            //        _isFiringMachineGun = true;
            //        InvokeRepeating("FireMachineGun", 0f, 1f / machineGun.fireRate);
            //        GameController.Instance.ShowBulletBarUI(true, gameObject.tag);
            //    }
            //} else {
            //    if ( Input.GetAxis(machineGunInput) >= 0f && _isFiringMachineGun ) {
            //        CancelInvoke("FireMachineGun");
            //        _isFiringMachineGun = false;
            //    }
            //}

            //Update camera follow speed
            GameController.Instance.UpdatePlayerCameraSpeed(gameObject.tag, _aeroplaneInput.IsAccelerating());

            //Fire Machinegun
            if ( machineGunDown && !_isFiringMachineGun ) {
                if ( !_isMachineGunOverHeated ) {
                    _isFiringMachineGun = true;
                    InvokeRepeating("FireMachineGun", 0f, 1f / machineGun.fireRate);
                    GameController.Instance.ShowBulletBarUI(true, gameObject.tag);
                }
            }

            //Stop Firing Machinegun
            if ( machineGunUp && _isFiringMachineGun ) {
                CancelInvoke("FireMachineGun");
                _isFiringMachineGun = false;
            }

            //Fire Smoke Rocket
            if ( specialWeapon01Down ) {
                //print("rocketInput");
                FireRocket(RocketType.SMOKE);
            }

            //Fire Special Weapon
            if ( rocketDown ) {
                FireRocket(RocketType.SIMPLE);
            }

            //Fire Special Weapon
            if ( specialWeapon02Down ) {
                FireRocket(RocketType.FIREWORKS);
            }

            //if ( Input.GetButtonDown(throttleInput) ) {
            //    //print("throttleInput");
            //    //FireMachineGun();
            //}

            //Release Smoke
            //if ( smokeDown ) {
            //    //print("smokeInput");
            //    InvokeRepeating("ReleaseSmoke", 0f, 1f / smokeDeployRate);
            //}

            //Show Rear View
            if ( rearViewDown ) {
                ShowRearView();
            }
        }

        //Stop releasing smoke
        //if ( smokeUp ) {
        //    CancelInvoke("ReleaseSmoke");
        //}

        //Hide Rear View
        if ( rearViewUp ) {
            HideRearView();
        }

        //Machinegun Cooldown
        if ( !_isFiringMachineGun ) {
            _mGCounter -= Time.deltaTime * machineGunCooldownSpeed;
            if ( _mGCounter <= 0 ) {
                _mGCounter = 0;
                GameController.Instance.ShowBulletBarUI(false, gameObject.tag);
                _isMachineGunOverHeated = false;
            } else {
                UpdateBulletsUI(false);
            }
        }


        //Weapon Selection
        //if ( selectedSW01 ) {
        //    //print("Selected 01 " + gameObject.name);
        //    _currentRocket = RocketType.SIMPLE;
        //    GameController.Instance.UpdateSelectionUI(gameObject.tag, new Vector3(0,0,0));
        //} else if ( selectedSW02 ) {
        //    //print("Selected 02");
        //    _currentRocket = RocketType.FIREWORKS;
        //    GameController.Instance.UpdateSelectionUI(gameObject.tag, new Vector3(0, 0, -90));
        //} else if ( selectedSW03 ) {
        //    //print("Selected 03");
        //    _currentRocket = RocketType.MINE;
        //    GameController.Instance.UpdateSelectionUI(gameObject.tag, new Vector3(0, 0, 180));
        //} else if ( selectedSW04 ) {
        //    //print("Selected 04");
        //    _currentRocket = RocketType.BARRIER;
        //    GameController.Instance.UpdateSelectionUI(gameObject.tag, new Vector3(0, 0, 90));
        //}

        //Rocket Cooldown
        //waits simpleRocketCooldown time to be able to shoot again
        if ( !_sRocketReady ) {
            _rocketTimer += Time.deltaTime;
            _sRocketCooldownCounter = 0f;
            if ( _rocketTimer > simpleRocketCooldown ) {
                _sRocketReady = true;
                _rocketTimer = 0f;
            }
        } else {
            // Simple Rocket Load Cooldown
            //when its able, wait simpleRocketLoadCooldown time to increase the amount of rockets available
            if ( _rocketCounter <= _currentRocketLoad && _rocketCounter > 0f ) {
                _sRocketCooldownCounter += Time.deltaTime;
                if ( _sRocketCooldownCounter > simpleRocketLoadCooldown ) {
                    _sRocketCooldownCounter = 0f;
                    _rocketCounter--;
                    UpdateRocketsUI();
                }
            }
        }

        // Smoke Rocket Cooldown
        //if ( !_smokeRocketReady ) {
        //    _sRocketCooldownCounter += Time.deltaTime;
        //    if ( _sRocketCooldownCounter > smokeRocketCooldown ) {
        //        _smokeRocketReady = true;
        //        _sRocketCooldownCounter = 0f;
        //    }
        //}

        //Special Weapon 01 Cooldown
        if ( !_sw01Ready ) {
            _sw01CooldownCounter += Time.deltaTime;
            if ( _sw01CooldownCounter > specialWeapon01Cooldown ) {
                _sw01Ready = true;
                _sw01CooldownCounter = 0f;
            }
        }

        //Special Weapon 02 Cooldown
        if ( !_sw02Ready ) {
            _sw02CooldownCounter += Time.deltaTime;
            if ( _sw02CooldownCounter > specialWeapon02Cooldown ) {
                _sw02Ready = true;
                _sw02CooldownCounter = 0f;
            }
        }

        //// Special Weapon 03 Cooldown
        //if ( !_sw03Ready ) {
        //    _sw03CooldownCounter += Time.deltaTime;
        //    if ( _sw03CooldownCounter > specialWeapon03Cooldown ) {
        //        _sw03Ready = true;
        //        _sw03CooldownCounter = 0f;
        //    }
        //}

        //// Special Weapon 04 Cooldown
        //if ( !_sw04Ready ) {
        //    _sw04CooldownCounter += Time.deltaTime;
        //    if ( _sw04CooldownCounter > specialWeapon04Cooldown ) {
        //        _sw04Ready = true;
        //        _sw04CooldownCounter = 0f;
        //    }
        //}
    }

    void FireMachineGun() {
        if ( _mGCounter > machineGun.maxLoad ) {
            _isMachineGunOverHeated = true;
            return;
        }

        _mGCounter++;

        //print("machineGunInput");
        muzzleFlash.Play();
        _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.SHOOT);

        UpdateBulletsUI(false);

        RaycastHit _hit;

        if ( Physics.SphereCast(camera.transform.position, machineGunCastRadious, camera.transform.forward, out _hit, machineGun.range, mask) ) {
            //Physics.Raycast(camera.transform.position, camera.transform.forward, out _hit, machineGun.range, mask)
            //print(_hit.collider.name);

            Vector3[] points = new[] { muzzleFlash.gameObject.transform.position, _hit.point };

            GameObject hitLine = Instantiate(bulletLine.gameObject, _hit.point, Quaternion.identity);
            hitLine.GetComponent<LineRenderer>().SetPositions(points);
            Destroy(hitLine, 0.5f);

            GameObject hitEffect = Instantiate(hitEffectPrefab, _hit.point, Quaternion.LookRotation(_hit.normal));
            Destroy(hitEffect, 2f);

            switch ( _hit.collider.tag ) {
                case "Player01":
                    _hit.collider.gameObject.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)machineGun.damage);
                    _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.HIT_SUCCESS);
                    break;
                case "Player02":
                    _hit.collider.gameObject.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)machineGun.damage);
                    _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.HIT_SUCCESS);
                    break;
                case "Smoke":
                    _hit.collider.gameObject.GetComponent<SmokeController>().TakeDamage(machineGun.damage);
                    break;
                case "Tank":
                    _hit.collider.gameObject.GetComponentInParent<TankController>().TakeDamage(machineGun.damage);
                    break;
                case "Balloon":
                    _hit.collider.gameObject.GetComponent<BalloonController>().TakeDamage(machineGun.damage);
                    break;
                default:
                    break;
            }
        }
    }

    void FireRocket(RocketType rocketType) {

        switch ( rocketType ) {
            case RocketType.SIMPLE:
                if ( _rocketCounter >= _currentRocketLoad ) {
                    return;
                }
                if ( !_sRocketReady ) {
                    return;
                }
                _sRocketReady = false;
                _rocketCounter++;

                GameObject newRocket = Instantiate(simpleRocketPrefab, rocketLauncherObject.transform.position, rocketLauncherObject.transform.rotation);
                //newRocket.transform.LookAt(rocketLauncherObject.transform.position);
                RocketController rocketController = newRocket.GetComponent<RocketController>();
                rocketController.SetDirection(camera.transform.forward);
                //rocketController.SetSpeed(rocketLauncher.speed);
                //rocketController.SetDamage(rocketLauncher.damage);
                rocketController.SetTag(gameObject.tag);
                break;          
            case RocketType.SMOKE:
                if ( !_sw01Ready ) {
                    return;
                }
                if ( _smokeCapacityCounter > smokeCapacity * smokeRocketCost ) {

                    _smokeCapacityCounter -= smokeCapacity * smokeRocketCost;

                    _sw01Ready = false;
                    GameObject newSmokeRocket = Instantiate(specialWeapon01Prefab, rocketLauncherObject.transform.position, rocketLauncherObject.transform.rotation);
                    //newRocket.transform.LookAt(rocketLauncherObject.transform.position);
                    RocketController smokeRocketController = newSmokeRocket.GetComponent<RocketController>();
                    smokeRocketController.SetDirection(camera.transform.forward);
                    //smokeRocketController.SetSpeed(smokeRocketLauncher.speed);
                    //smokeRocketController.SetDamage(smokeRocketLauncher.damage);
                    smokeRocketController.SetTag(gameObject.tag);

                    UpdateSmokeUI();
                }
                break;
            case RocketType.FIREWORKS:

                if ( !_sw02Ready ) {
                    return;
                }
                if ( _smokeCapacityCounter > smokeCapacity * smokeRocketCost ) {

                    _smokeCapacityCounter -= smokeCapacity * smokeRocketCost;

                    _sw02Ready = false;
                    GameObject newFireworksRocket = Instantiate(specialWeapon02Prefab, rocketLauncherObject.transform.position, rocketLauncherObject.transform.rotation);
                    FireworksController fireworksController = newFireworksRocket.GetComponent<FireworksController>();
                    fireworksController.SetDirection(camera.transform.forward);
                    fireworksController.SetTag(gameObject.tag);
                    fireworksController.fireworksObject = specialWeapon02Prefab;

                    //rocketController.isChild = true;
                    //rocketController.explosionTime = explosionTime / 4;

                    UpdateSmokeUI();
                }

                break;
            default:
                break;
        }

                //case RocketType.BARRIER:

                //    if ( !_sw04Ready ) {
                //        return;
                //    }
                //    _sw04Ready = false;

                //    GameObject newBarrierRocket = Instantiate(specialWeapon04Prefab, rocketLauncherObject.transform.position, rocketLauncherObject.transform.rotation);
                //    RocketController barrierController = newBarrierRocket.GetComponent<RocketController>();
                //    barrierController.SetDirection(camera.transform.forward);
                //    barrierController.SetTag(gameObject.tag);
                //    break;

             

        _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.ROCKET);
        UpdateRocketsUI();
    }

    //void FireSpecialWeapon() {
    //    print("fire special");
    //    switch ( _currentRocket ) {
    //        case RocketType.SIMPLE:
    //            _sw01Ready = false;
    //            FireRocket();
    //            break;
    //        case RocketType.BARRIER:
    //            break;
    //        case RocketType.SMOKE:
    //            break;
    //        default:
    //            break;
    //    }
    //}

    void ReleaseSmoke() {

        if ( _smokeCapacityCounter > 0 ) {

            GameObject newSmoke = Instantiate(smokeObject, smokeLauncherObject.transform.position, Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
            //_smokeCapacityCounter--;
            newSmoke.GetComponent<SmokeController>().SetParentTag(gameObject.tag);
            //_aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.SMOKE);
            UpdateSmokeUI();

        } else {
            //_smokeCapacityCounter = 0;
            CancelInvoke("ReleaseSmoke");
        }
    }

    //void ReleaseSmokeFromPool() {
    //    if ( _smokeCapacityCounter > 0 ) {            
    //        _smokeList[_smokeCount].SetActive(true);
    //        _smokeList[_smokeCount].transform.position = smokeLauncherObject.transform.position;
    //        _smokeList[_smokeCount].transform.rotation = Quaternion.Euler(Random.Range(0,360), Random.Range(0, 360), Random.Range(0, 360));
    //        _smokeCapacityCounter--;
    //        _smokeCount++;
    //        if ( _smokeCount == _smokeList.Count ) {
    //            _smokeCount = 0;
    //        }

    //        UpdateSmokeUI();

    //    } else {
    //        //_smokeCapacityCounter = 0;
    //        CancelInvoke("ReleaseSmokeFromPool");
    //    }
    //}

    //void UpdateBulletsUI() {
    //    bulletsText.text = (machineGun.maxLoad - _mGCounter) > 0 ? (machineGun.maxLoad - _mGCounter).ToString() : "Reloading";
    //}

    void UpdateBulletsUI(bool isCoolingDown) {
        if ( isCoolingDown ) {
            //GameController.Instance.UpdateBulletBarUI((float)_mGCooldownCounter  / (float)machineGunCooldownRate, gameObject.tag);
        } else {
            GameController.Instance.UpdateBulletBarUI((float)_mGCounter / (float)machineGun.maxLoad, gameObject.tag);
        }
    }

    void UpdateRocketsUI() {
        rocketText.text = (_currentRocketLoad - _rocketCounter) > 0 ? (_currentRocketLoad - _rocketCounter).ToString() : "0";
        //switch ( _currentRocket ) {
        //    case RocketType.SIMPLE:
        //        rocketUIImage.sprite = simpleRocketImage;
        //        break;
        //    case RocketType.BARRIER:
        //        rocketUIImage.sprite = tripleRocketImage;
        //        break;
        //    default:
        //        break;
        //}

    }

    void UpdateSmokeUI() {
        if ( _smokeCapacityCounter >= 0 )
            smokeBar.fillAmount = (float)_smokeCapacityCounter / (float)smokeCapacity;

        //print(_smokeCapacityCounter +" " +smokeCapacity +" " + (float)_smokeCapacityCounter / (float)smokeCapacity);
    }

    void UpdateLifeUI() {
        if ( _currentHealth >= 0 )
            //lifeBar.localScale = new Vector3((float)_currentHealth / (float)maxHealth, 1f, 1f);
            lifeBar.fillAmount = (float)_currentHealth / (float)maxHealth;

        for ( int i = 0; i < lifeIcons.Length; i++ ) {
            if ( i < _currentLifes ) {
                lifeIcons[i].SetActive(true);
            } else {
                lifeIcons[i].SetActive(false);
            }
        }
    }

    public void TakeDamage(int damage) {
        _currentHealth -= damage;
        _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.HIT);
        if ( _currentHealth <= 0 ) {
            DisableColliders();
            _enabled = false;
            _currentLifes--;
            _currentHealth = maxHealth;
            CancelInvoke("ReleaseSmoke");
            CancelInvoke("FireMachineGun");
            GameObject expEffect = Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(expEffect, 5f);
            GameObject _pilot = Instantiate(pilot, transform.position, Quaternion.identity);
            _pilot.transform.LookAt(transform.forward);
            GameController.Instance.RespawnPlayer(gameObject.tag, _pilot);
            HideRearView();
        } else {
            _collided = false;
        }
        UpdateLifeUI();

        if ( _currentLifes <= 0 ) {
            GameController.Instance.ShowGameOverUI(gameObject.tag);
        }
    }

    public void Respawn() {
        _enabled = true;
        _collided = false;
        EnableColliders();
        _mGCounter = 0;
        UpdateBulletsUI(false);
        _canFireRocket = true;
        _rocketTimer = 0f;
        _rocketCounter = 0;
        UpdateRocketsUI();
        _smokeCapacityCounter = smokeCapacity;
        UpdateSmokeUI();
        _aeroplaneAudio.ResetAudio();
        _aeroplaneController.StartEngine();
        transform.position = spawnBox.GetRandomPosition();

        DisableColliders();

        InvokeRepeating("Blink", 0f, 1f / blinkRate);
        Invoke("EnableColliders", invulnerabilityTime);
        InvokeRepeating("ReleaseSmoke", invulnerabilityTime, 1f / smokeDeployRate);

    }

    private void EnableColliders() {
        colliders.SetActive(true);
        CancelInvoke("Blink");
        fuselage.SetActive(true);
    }

    private void DisableColliders() {
        colliders.SetActive(false);
    }

    private void Blink() {
        fuselage.SetActive(_blinkOn);
        _blinkOn = !_blinkOn;
    }

    private void ShowRearView() {
        rearCameraPivot.SetActive(true);
    }

    private void HideRearView() {
        rearCameraPivot.SetActive(false);
    }

    private void OnTriggerEnter(Collider other) {
        switch ( other.tag ) {
            case "Smoke":
                if ( !_collided ) {
                    SmokeController smoke = other.gameObject.GetComponent<SmokeController>();
                    if (!gameObject.CompareTag(smoke.GetParentTag())) {
                        //DisableColliders();
                        //Invoke("EnableColliders", _invulnerabilityTime);
                        TakeDamage(maxHealth);
                        Destroy(other.gameObject);
                        _collided = true;
                    }                    
                }
                break;
            //case "RocketItem":
            //    _rocketCounter = 0;
            //    _currentRocketLoad = simpleRocketMaxLoad;
            //    _currentRocket = RocketType.SIMPLE;
            //    UpdateRocketsUI();
            //    Destroy(other.gameObject);
            //    _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.PICKUP);
            //    //print("got rocket");
            //    break;
            case "SmokeItem":
                //print("got smoke");
                _smokeCapacityCounter = smokeCapacity;
                UpdateSmokeUI();
                Destroy(other.gameObject);
                _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.PICKUP);
                break;
            case "SmokeItemHalf":
                //print("got smoke");
                _smokeCapacityCounter = (_smokeCapacityCounter + smokeCapacity / 2 < smokeCapacity) ? _smokeCapacityCounter + smokeCapacity / 2 : smokeCapacity;
                UpdateSmokeUI();
                Destroy(other.gameObject);
                _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.PICKUP);
                break;
            //case "TripleRocketItem":
            //    _rocketCounter = 0;
            //    _currentRocketLoad = tripleRocket.maxLoad;
            //    _currentRocket = RocketType.BARRIER;
            //    UpdateRocketsUI();
            //    Destroy(other.gameObject);
            //    _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.PICKUP);
            //    break;
            //case "SmokeRocketItem":
            //    _rocketCounter = 0;
            //    _currentRocketLoad = smokeRocketLauncher.maxLoad;
            //    _currentRocket = RocketType.SMOKE;
            //    UpdateRocketsUI();
            //    Destroy(other.gameObject);
            //    _aeroplaneAudio.PlayEffect(AeroplaneAudio.SoundType.PICKUP);
            //    //print("got rocket");
            //    break;
            default:
                break;
        }
    }

    private void OnCollisionEnter(Collision collision) {
        switch ( collision.gameObject.tag ) {

            case "Smoke":
                if ( !_collided ) {
                    //DisableColliders();
                    //Invoke("EnableColliders", _invulnerabilityTime);
                    TakeDamage(maxHealth);
                    Destroy(collision.gameObject);
                    _collided = true;
                }
                break;
            case "Wall":
                TakeDamage(wallDamage);
                GameObject expEffect = Instantiate(explosionEffect, transform.position, transform.rotation);
                Destroy(expEffect, 5f);
                break;
            default:
                break;
        }
    }
}
