﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataPersistence : MonoBehaviour {

	// Use this for initialization
	public static PlayerData loadData() {
        float volumeLoaded = PlayerPrefs.GetFloat("volumeGeneral");
        int qualityLoaded = PlayerPrefs.GetInt("qualityGeneral");

        PlayerData playerData = new PlayerData
        {
            volumeGeneral = volumeLoaded,
            qualityGeneral = qualityLoaded
        };

        return playerData;
    }
    public static void SaveData(string kind, string name, string value)
    {
        switch (kind)
        {
            case "string":
                PlayerPrefs.SetString(name, value);
            break;
            case "float":
                PlayerPrefs.SetFloat(name, float.Parse(value));
            break;
            case "int":
                PlayerPrefs.SetInt(name, int.Parse(value));
            break;
        }
    }
}
