﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour {

    public bool spawnSmoke;
    public GameObject[] spawnables;
    public GameObject smokePrefab;
    public Transform maxPoint;
    public Transform minPoint;
    public float spawnRate;
    public float smokeSpawnRate;
    public float overTime;

    private float _timer = 0;
    private float _smokeTimer = 0;
    private float _generalTimer = 0;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _generalTimer += Time.deltaTime;
        if ( spawnSmoke ) {
            if ( _generalTimer > overTime ) {
                SpawnSmoke();
                SpawnItem();
            } else {
                SpawnItem();
            }
        } else {
            SpawnItem();
        }
    }

    void SpawnItem() {
        if ( _timer >= spawnRate ) {
            Instantiate(spawnables[Random.Range(0, spawnables.Length)],
                new Vector3(Random.Range(minPoint.position.x, maxPoint.position.x), Random.Range(minPoint.position.y, maxPoint.position.y), Random.Range(minPoint.position.z, maxPoint.position.z)),
                Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
            _timer = 0f;
        }
        _timer += Time.deltaTime;
    }

    void SpawnSmoke() {
            if ( _smokeTimer >= smokeSpawnRate ) {
                Instantiate(smokePrefab,
                    new Vector3(Random.Range(minPoint.position.x, maxPoint.position.x), Random.Range(minPoint.position.y, maxPoint.position.y), Random.Range(minPoint.position.z, maxPoint.position.z)),
                    Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
                _smokeTimer = 0f;
            }
        _smokeTimer += Time.deltaTime;
    }
}
