﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeRocketController : RocketController{

    public GameObject smokeObject;
    public float delayTime;
    public float deployRate;
    public float positionOffset;

    // Use this for initialization
    void Start () {
        InvokeRepeating("ReleaseSmoke", delayTime, deployRate);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void ReleaseSmoke() {       

            GameObject newSmoke = Instantiate(smokeObject, transform.position - positionOffset * GetDirection(), Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));

    }
}
