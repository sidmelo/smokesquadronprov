using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Aeroplane
{
    [RequireComponent(typeof (AeroplaneController))]
    public class AeroplaneUserControl2Axis : MonoBehaviour
    {
        // these max angles are only used on mobile, due to the way pitch and roll input are handled

        public bool disableInput = false;
        public float maxRollAngle = 80;
        public float maxPitchAngle = 80;

        public string rollInputAxis = "Horizontal";
        public string pitchInputAxis = "Vertical";
        public string yawInput = "Yaw1";
        public string throttleInput = "Throtle1";

        // reference to the aeroplane that we're controlling
        private AeroplaneController m_Aeroplane;

        private bool _isAccelerating;

        private void Awake()
        {
            // Set up the reference to the aeroplane controller.
            m_Aeroplane = GetComponent<AeroplaneController>();
        }


        private void FixedUpdate()
        {
            // Read input for the pitch, yaw, roll and throttle of the aeroplane.
            float roll = CrossPlatformInputManager.GetAxis(rollInputAxis);
            float pitch = CrossPlatformInputManager.GetAxis(pitchInputAxis);
            float yaw = CrossPlatformInputManager.GetAxis(yawInput);
            //_isAccelerating = CrossPlatformInputManager.GetButton(throttleInput);
            //if ( Input.GetAxis(throttleInput) < 0f ) {
            //    _isAccelerating = true;
            //} else _isAccelerating = false;

            _isAccelerating = (Input.GetAxis(throttleInput) < 0f) ? true : false;


            // auto throttle up, or down if braking.
            //float throttle = airBrakes ? -1 : 1;
#if MOBILE_INPUT
            AdjustInputForMobileControls(ref roll, ref pitch, ref throttle);
#endif
            // Pass the input to the aeroplane
            if ( !disableInput)
                m_Aeroplane.Move(roll, pitch, yaw, 1, false, _isAccelerating);
            else
                m_Aeroplane.Move(0, 0, 0, 1, false, false);
        }

        public bool IsAccelerating() {
            return _isAccelerating;
        }


        private void AdjustInputForMobileControls(ref float roll, ref float pitch, ref float throttle)
        {
            // because mobile tilt is used for roll and pitch, we help out by
            // assuming that a centered level device means the user
            // wants to fly straight and level!

            // this means on mobile, the input represents the *desired* roll angle of the aeroplane,
            // and the roll input is calculated to achieve that.
            // whereas on non-mobile, the input directly controls the roll of the aeroplane.

            float intendedRollAngle = roll*maxRollAngle*Mathf.Deg2Rad;
            float intendedPitchAngle = pitch*maxPitchAngle*Mathf.Deg2Rad;
            roll = Mathf.Clamp((intendedRollAngle - m_Aeroplane.RollAngle), -1, 1);
            pitch = Mathf.Clamp((intendedPitchAngle - m_Aeroplane.PitchAngle), -1, 1);

            // similarly, the throttle axis input is considered to be the desired absolute value, not a relative change to current throttle.
            float intendedThrottle = throttle*0.5f + 0.5f;
            throttle = Mathf.Clamp(intendedThrottle - m_Aeroplane.Throttle, -1, 1);
        }
    }
}
