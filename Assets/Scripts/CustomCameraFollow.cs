﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCameraFollow : MonoBehaviour {

    public Transform target;
    public float moveSpeed = 0.2f;
    public float fastMoveSpeed = 0.2f;
    public float rotateSpeed = 0.2f;

    private float _speed;

    // Use this for initialization
    void Start () {
        _speed = moveSpeed;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate() {
        if ( !target )
            return;

        //float wantedRotationAngleSide = target.eulerAngles.y;
        //float currentRotationAngleSide = transform.eulerAngles.y;

        //float wantedRotationAngleUp = target.eulerAngles.x;
        //float currentRotationAngleUp = transform.eulerAngles.x;

        //currentRotationAngleSide = Mathf.LerpAngle(currentRotationAngleSide, wantedRotationAngleSide, rotationDamping * Time.deltaTime);

        //currentRotationAngleUp = Mathf.LerpAngle(currentRotationAngleUp, wantedRotationAngleUp, rotationDamping * Time.deltaTime);

        //Quaternion currentRotation = Quaternion.Euler(currentRotationAngleUp, currentRotationAngleSide, 0);

        //transform.position = target.position;
        //transform.position -= currentRotation * Vector3.forward * distance;

        //transform.LookAt(target);

        //transform.position += transform.up * height;

        Vector3 targetPosition = target.position;
        Quaternion targetRotation = target.rotation;

        transform.position = transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * _speed);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);

    }

    public void Accelerate(bool isAccelerating) {
        _speed = isAccelerating ? fastMoveSpeed : moveSpeed;
    }
}
