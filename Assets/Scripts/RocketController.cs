﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{

    public GameObject explosionParticle;


    [SerializeField] private float _damage;
    private Vector3 _direction;
    [SerializeField] private float _speed;
    private string _tag;

    private Rigidbody _rigidbody;

    public void SetSpeed(float speed) {
        _speed = speed;
    }

    public void SetDirection(Vector3 direction) {
        _direction = direction;
    }

    public void SetDamage(float damage) {
        _damage = damage;
    }

    public float GetDamage() {
      return  _damage;
    }

    public void SetTag(string tag) {
        _tag = tag;
    }

    public string GetTag() {
       return _tag;
    }

    public Vector3 GetDirection() {
        return _direction;
    }
    // Use this for initialization
    void Start() {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate() {

        //transform.Translate(_direction * Time.deltaTime * _speed);
        //transform.rotation = Quaternion.LookRotation(_direction);
        transform.position += _direction * Time.deltaTime * _speed;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != _tag && other.gameObject.tag != "ProximitySensor") {

            switch (other.tag) {
                case "Player01":
                    other.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)_damage);
                    break;
                case "Player02":
                    other.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)_damage);
                    break;
                case "Smoke":
                    Destroy(other.gameObject);
                    break;
                case "Tank":
                    other.GetComponentInParent<TankController>().TakeDamage((int)_damage);
                    break;
                case "Balloon":
                    other.GetComponent<BalloonController>().TakeDamage((int)_damage);
                    break;
                default:
                    break;
            }

            //if ( other.gameObject.tag == "Player01" || other.gameObject.tag == "Player02" ) {
            //    other.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)_damage);
            //}

            //if ( other.gameObject.tag == "Smoke" ) {
            //    Destroy(other.gameObject);
            //}

            //print("bateu no rocket");
            //print(other.gameObject.name);

            GameObject explosionEffect = Instantiate(explosionParticle, transform.position, Quaternion.LookRotation(transform.up));
            Destroy(explosionEffect, 2f);

            Destroy(this.gameObject);
        }
    }
}

