﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

    [SerializeField] private string _rollAxis = "Horizontal";
    [SerializeField] private string _pitchAxis = "Vertical";
    [SerializeField] private string _special = "Special";
    [SerializeField] private string _throttle = "Throtle";
    [SerializeField] private string _yawAxis = "Yaw";
    [SerializeField] private string _machineGun = "FireTrigger";
    [SerializeField] private string _rocket = "RocketTrigger";
    [SerializeField] private string _sw01 = "SW01Trigger";
    [SerializeField] private string _sw02 = "SW02Trigger";
    [SerializeField] private string _smoke = "Smoke";
    [SerializeField] private string _rearView = "Rear";
    [SerializeField] private string _swHorizontal = "SWHorizontal";
    [SerializeField] private string _swVertical = "SWVertical";

    public string RollAxis { get { return _rollAxis; } }
    public string PitchAxis { get { return _pitchAxis; } }
    public string Special { get { return _special; } }
    public string Throttle { get { return _throttle; } }
    public string YawAxis { get { return _yawAxis; } }
    public string MachineGun { get { return _machineGun; } }
    public string Rocket { get { return _rocket; } }
    public string Smoke { get { return _smoke; } }
    public string RearView { get { return _rearView; } }
    public string SWHorizontal { get { return _swHorizontal; } }
    public string SWVertical { get { return _swVertical; } }
    public string SW02 { get { return _sw02; } }
    public string SW01 { get { return _sw01; }  }

    public int maxNumPlayers = 4;
    public int maxNumInputs = 2;

    [HideInInspector] public static InputHandler Instance;

    private List<int> _playerControllers;
    private int _assignedPlayers = 0;
    private static bool created = false;
    private bool _isAssigningInput = false;


    void Awake() {
        if ( !created ) {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            Instance = this;

            _playerControllers = new List<int>();
            // Debug.Log("Awake: " + this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if (_isAssigningInput && _assignedPlayers < maxNumPlayers) {
            for ( int i = 0; i < maxNumInputs; i++ ) {
                if ( Input.GetButtonDown(InputHandler.Instance.MachineGun + i) ) {
                    if (!_playerControllers.Contains(i)) {
                        _playerControllers.Add(i);
                        _assignedPlayers++;
                        print("assigned " + i);
                    }
                }
            }
        }        
	}

    public void AssignPlayerInput() {
        _playerControllers.Clear();
        _isAssigningInput = true;
        _assignedPlayers = 0;
    }

    public void FinishAssignment() {
        _isAssigningInput = false;
    }

    /// <summary>
    /// Player 1 has index 0
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public int GetPlayerInputNumber(int index) {
        return _playerControllers[index];
    }

    public int GetNumControllers() {
        return _playerControllers.Count;
    }
}
