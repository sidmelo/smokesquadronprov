﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireworksController : RocketController {
    // Start is called before the first frame update

    public float explosionTime;
    public float childNumber;
    public bool isChild;
    public GameObject smokeObject;
    public GameObject fireworksObject;

    void Start() {
        Invoke("Explode", explosionTime);
    }

    // Update is called once per frame
    void Update() {

    }

    void Explode() {
        if (!isChild) {
            for ( int i = 0; i < childNumber; i++ ) {
                GameObject newRocket = Instantiate(fireworksObject, transform.position, Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
                FireworksController rocketController = newRocket.GetComponent<FireworksController>();
                rocketController.SetDirection(newRocket.transform.forward);
                rocketController.SetTag(GetTag());
                rocketController.isChild = true;
                rocketController.explosionTime = explosionTime / 4;
                rocketController.GetComponent<CapsuleCollider>().enabled = false;

            }            
        }
        GameObject newSmoke = Instantiate(smokeObject, transform.position, Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)));
        GameObject explosionEffect = Instantiate(explosionParticle, transform.position, Quaternion.LookRotation(transform.up));
        Destroy(explosionEffect, 2f);

        Destroy(this.gameObject);
    }
}
