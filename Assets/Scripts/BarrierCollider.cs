﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierCollider : MonoBehaviour
{

    public BarrierRocketController barrierController;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    private void OnTriggerEnter(Collider other) {
        if ( other.gameObject.tag != barrierController.GetTag() ) {

            switch ( other.tag ) {
                case "Player01":
                    other.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)barrierController.GetDamage());
                    break;
                case "Player02":
                    other.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)barrierController.GetDamage());
                    break;
                case "Smoke":
                    Destroy(other.gameObject);
                    break;
                case "Tank":
                    other.GetComponentInParent<TankController>().TakeDamage((int)barrierController.GetDamage());
                    break;
                case "Balloon":
                    other.GetComponent<BalloonController>().TakeDamage((int)barrierController.GetDamage());
                    break;
                default:
                    break;
            }

            //if ( other.gameObject.tag == "Player01" || other.gameObject.tag == "Player02" ) {
            //    other.GetComponentInParent<AeroplaneWeaponController>().TakeDamage((int)_damage);
            //}

            //if ( other.gameObject.tag == "Smoke" ) {
            //    Destroy(other.gameObject);
            //}

            //GameObject explosionEffect = Instantiate(explosionParticle, transform.position, Quaternion.LookRotation(transform.up));
            //Destroy(explosionEffect, 2f);

            //Destroy(this.gameObject);

            print("bateu na parede");
        }
    }
}
