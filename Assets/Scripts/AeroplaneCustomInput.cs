﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Aeroplane;

public class AeroplaneCustomInput : MonoBehaviour {


    public bool disableInput = false;
    public int playerNumber;
    
    //public string rollInputAxis = "Horizontal";
    //public string pitchInputAxis = "Vertical";
    //public string airBrakesInput = "AirBrake1";
    //public string throttleInput = "Throtle1";
    //public string yawInput = "Yaw1";

    //public string machineGunInput = "FireTrigger1";
    //public string rocketInput = "RocketTrigger1";
    //public string smokeInput = "Smoke1";
    //public string rearViewInput = "Rear1";
    
    private bool _isAccelerating;
    private int _inputNumber = 0;
    // reference to the aeroplane that we're controlling
    private AeroplaneController _aeroplane;
    private AeroplaneWeaponController _weaponController;

    // Use this for initialization
    void Awake() {
        _aeroplane = GetComponent<AeroplaneController>();
        _weaponController = GetComponent<AeroplaneWeaponController>();
        Debug.Log(playerNumber);
        if ( !disableInput )
            _inputNumber = InputHandler.Instance.GetPlayerInputNumber(playerNumber);
    }

    private void Update() {
        _weaponController.machineGunDown = Input.GetButtonDown(InputHandler.Instance.MachineGun + _inputNumber);
        _weaponController.machineGunUp = Input.GetButtonUp(InputHandler.Instance.MachineGun + _inputNumber);
        _weaponController.rocketDown = Input.GetButtonDown(InputHandler.Instance.Rocket + _inputNumber);
        _weaponController.specialWeapon01Down = Input.GetButtonUp(InputHandler.Instance.SW01 + _inputNumber);
        _weaponController.specialWeapon02Down = Input.GetButtonUp(InputHandler.Instance.SW02 + _inputNumber);
        //_weaponController.smokeDown = Input.GetButtonDown(InputHandler.Instance.Smoke + _inputNumber);
        // _weaponController.smokeUp = Input.GetButtonUp(InputHandler.Instance.Smoke + _inputNumber);
        _weaponController.rearViewDown = Input.GetButtonDown(InputHandler.Instance.RearView + _inputNumber);
        _weaponController.rearViewUp = Input.GetButtonUp(InputHandler.Instance.RearView + _inputNumber);
        //_weaponController.selectedSW01 = (Input.GetAxis(InputHandler.Instance.SWVertical + _inputNumber) == 1f);
        //_weaponController.selectedSW02 = (Input.GetAxis(InputHandler.Instance.SWHorizontal + _inputNumber) == 1f);
        //_weaponController.selectedSW03 = (Input.GetAxis(InputHandler.Instance.SWVertical + _inputNumber) == -1f);
        //_weaponController.selectedSW04 = (Input.GetAxis(InputHandler.Instance.SWHorizontal + _inputNumber) == -1f);


        //_weaponController.specialWeaponDown = Input.GetButtonDown(InputHandler.Instance.Special + _inputNumber);

        if ( !disableInput ) {
            _weaponController.HandleWeapons();
        }
    }

    private void FixedUpdate() {
        // Read input for the pitch, yaw, roll and throttle of the aeroplane.
        
        float yaw = Input.GetAxis(InputHandler.Instance.YawAxis + _inputNumber);
        float roll = Input.GetAxis(InputHandler.Instance.RollAxis + _inputNumber);
        float pitch = Input.GetAxis(InputHandler.Instance.PitchAxis + _inputNumber);
        bool airBrakes = (Input.GetAxis(InputHandler.Instance.Throttle + _inputNumber) == 1f) ? true : false;
        //bool isAccelerating = Input.GetButton(PlayerInput.throttle + inputNumber);
        //_isAccelerating = CrossPlatformInputManager.GetButton(throttleInput);
        //if ( Input.GetAxis(throttleInput) < 0f ) {
        //    _isAccelerating = true;
        //} else _isAccelerating = false;

        _isAccelerating = (Input.GetAxis(InputHandler.Instance.Throttle + _inputNumber) < 0f) ? true : false;

        // auto throttle up, or down if braking.
        //float throttle = airBrakes ? -1 : 1;

        // Pass the input to the aeroplane
        if ( !disableInput ) {
            _aeroplane.Move(roll, pitch, yaw, 1, airBrakes, _isAccelerating);
            _weaponController.HandleWeapons();
        } else{
            _aeroplane.Move(0, 0, 0, 1, false, false);
        }            
    }

    public bool IsAccelerating() {
        return _isAccelerating;
    }
}
