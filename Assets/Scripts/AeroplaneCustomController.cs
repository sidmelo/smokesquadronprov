﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AeroplaneCustomController : MonoBehaviour {

    public float acceleration; // valor padrao de aceleraçao e deve ser balanceado
    public float turnSpeed; // valor padrao de velocidade de curva e deve ser balanceado
    public float maxSpeed; // valor padrao de velocidade maxima e deve ser balanceado
    public float minSpeed; // valor padrao de velocidade minima e deve ser balanceado
    public float lerpFactor;

    private float _currentForwardSpeed; // valor padrao deve ser a velocidade inicial e igual a velocidade minima
    private float _currentYawSpeed; // valor padrao deve ser 0
    private float _currentPitchSpeed; // valor padrao deve ser 0 
    private float _currentRollSpeed; // valor padrao deve ser 0
    private Rigidbody _rigidbody;

    private float _rollInput;
    private float _pitchInput;
    private float _yawInput;
    private float _throttleInput;
    private bool _isAccelerating;

    // Use this for initialization
    void Awake () {
        _currentForwardSpeed = minSpeed; 
        _currentYawSpeed = 0f;
        _currentPitchSpeed = 0f;
        _currentRollSpeed = 0f;
        _rigidbody = GetComponent<Rigidbody>();
    }
	

    public void Move(float rollInput, float pitchInput, float yawInput, bool isAccelerating) {
        _rollInput = rollInput;
        _pitchInput = pitchInput;
        _yawInput = yawInput;
        _isAccelerating = isAccelerating;

        // adiciona posiçao pra frente (x,y,z) sendo x para frente
        //transform.position += transform.forward * _currentForwardSpeed * Time.deltaTime;
        Vector3 newVelocity = Vector3.Lerp(_rigidbody.velocity, transform.forward * _currentForwardSpeed, _currentForwardSpeed * lerpFactor * Time.deltaTime);
        _rigidbody.velocity = newVelocity;

        //transform.position = Vector3.Lerp(transform.position, transform.position + transform.forward * _currentForwardSpeed * Time.deltaTime, Time.deltaTime * lerpFactor);

        //transform.Rotate(new Vector3(_currentRollSpeed * Time.deltaTime, _currentPitchSpeed * Time.deltaTime, _currentYawSpeed * Time.deltaTime));

        Quaternion deltaRotation = Quaternion.Euler(new Vector3(_currentPitchSpeed, _currentYawSpeed, _currentRollSpeed) * Time.deltaTime);
        _rigidbody.MoveRotation(_rigidbody.rotation * deltaRotation);

        Thrust(); // input dos motores

        MoveUp(); // input pitch, movimentaçao para cima e para baixo

        MoveRight(); // input roll e yaw, movimentaçao lateral direita e esquerda       
    }

    private void Thrust() {// aceleraçao extra com shift e desaceleraçao automatica

        if ( _isAccelerating ) {
            _currentForwardSpeed = Mathf.Clamp(_currentForwardSpeed + (acceleration ), minSpeed, maxSpeed);
            // clamp( valor recebido, valor minimo, valor maximo)
        } else {
            _currentForwardSpeed = Mathf.Clamp(_currentForwardSpeed + (acceleration * -0.5f * Time.deltaTime), minSpeed, maxSpeed);
        }
    }

    private void MoveUp() {// input pitch, movimentaçao para cima e para baixo
        if ( _pitchInput > 0){

            _currentPitchSpeed = Mathf.Lerp(_currentPitchSpeed, turnSpeed * Time.deltaTime, Time.deltaTime * 2f);

        } else if( _pitchInput < 0){
            _currentPitchSpeed = Mathf.Lerp(_currentPitchSpeed, turnSpeed * -1 * Time.deltaTime, Time.deltaTime * 2f);            
            // se input invertido: trocar -1 entre os 2 inputs

        } else{
            _currentPitchSpeed = 0;
        }
    }

    public void MoveRight() {// input roll e yaw, movimentaçao lateral direita e esquerda
        if ( _rollInput < 0){

            _currentRollSpeed = Mathf.Lerp(_currentRollSpeed, turnSpeed * Time.deltaTime, Time.deltaTime * 2f);
            // rotaçao principal do roll direita

            //_currentYawSpeed = Mathf.Lerp(_currentYawSpeed, turnSpeed * 0.2 * Time.deltaTime, Time.deltaTime * 2f);
            // rotaçao diminuida no yaw direita
        } else if( _rollInput > 0 ) {

            _currentRollSpeed = Mathf.Lerp(_currentRollSpeed, turnSpeed * -1f * Time.deltaTime, Time.deltaTime * 2f);
            // rotaçao principal do roll esquerda

            //_currentYawSpeed = Mathf.Lerp(_currentYawSpeed, turnSpeed * -0.2f * Time.deltaTime, Time.deltaTime * 2f);
            // rotaçao diminuida no yaw esquerda
        } else{
            _currentRollSpeed = 0;
            _currentYawSpeed = 0;
        }

    }
}

/*
 float Acceleration; // valor padrao de aceleraçao e deve ser balanceado
float Turn Speed; // valor padrao de velocidade de curva e deve ser balanceado
float Max Speed; // valor padrao de velocidade maxima e deve ser balanceado
float Min Speed; // valor padrao de velocidade minima e deve ser balanceado
float Current forward Speed; // valor padrao deve ser a velocidade inicial e igual a velocidade minima
float Current yaw Speed; // valor padrao deve ser 0
float Current pitch Speed; // valor padrao deve ser 0 
float Current roll Speed; // valor padrao deve ser 0



Update(){

	AddLocalOffset(Current Forward Speed*delta seconds, 0 , 0) // adiciona posiçao pra frente (x,y,z) sendo x para frente
	
	AddLocalRotation(Current roll Speed*delta seconds, Current pitch Speed*delta seconds,Current yaw Speed*delta seconds)


	InputThrust(); // input dos motores
	
	InputMoveUp(); // input pitch, movimentaçao para cima e para baixo

	InputMoveRight(); // input roll e yaw, movimentaçao lateral direita e esquerda


}

Void InputThrust(){  // aceleraçao extra com shift e desaceleraçao automatica

	if(input pra frente){

		Current forward Speed = clampFloat( Current forward Speed + (Acceleration* delta seconds), Min Speed , Max Speed); 
		// clamp( valor recebido, valor minimo, valor maximo)
	}

	if(input pra frente nao pressionado){

		
		Current forward Speed = clampFloat( Current forward Speed + (Acceleration *  -0,5 * delta seconds), Min Speed , Max Speed);

	}


}


void MoveUp(){

	if(input pra cima){

		Current pitch Speed = FInterpTo(Current pitch Speed, Turn Speed*Delta seconds, Delta seconds, 2);
		// FInterpTo é uma funçao que interpola entre 2 valores float dado um tempo (current, target, delta time, interp time)

	}

		elseif(input pra baixo){

			Current pitch Speed = FInterpTo(Current pitch Speed, Turn Speed * -1 * Delta seconds, Delta seconds, 2);
			// se input invertido: trocar -1 entre os 2 inputs

		}

			else{
				Current pitch Speed = 0
			}

}


void MoveRight(){

	if(input pra direita){

		Current roll Speed = FInterpTo(Current roll Speed, Turn Speed * Delta seconds, Delta seconds, 2);
		// rotaçao principal do roll direita


		Current yaw Speed = FInterpTo(Current yaw Speed, Turn Speed * 0.2 * Delta seconds, Delta seconds, 2);
		// rotaçao diminuida no yaw direita
	}

		elseif(input pra esquerda){

			Current roll Speed = FInterpTo(Current roll Speed, Turn Speed * -1 * Delta seconds, Delta seconds, 2);
			// rotaçao principal do roll esquerda


			Current yaw Speed = FInterpTo(Current yaw Speed, Turn Speed * -0.2 * Delta seconds, Delta seconds, 2);
			// rotaçao diminuida no yaw esquerda
		}	

			else{
				Current roll Speed = 0
				Current yaw Speed = 0
			}
	
}

     */
