﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGizmo : MonoBehaviour {

    public Camera camera;
    public GameObject player;

    private RectTransform _rect;
	// Use this for initialization
	void Start () {
        _rect = GetComponent<RectTransform>();

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 screenPos = camera.WorldToScreenPoint(player.transform.position);
        transform.position = screenPos;
    }
}
