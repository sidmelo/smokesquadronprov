﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Utility;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour {

    public static GameController Instance;

    [Header("Players")]
    public GameObject[] players;
    public float respawnTime = 3f;

    [Header("Cameras")]
    public CustomCameraFollow[] camerasFollow;
    public Camera[] cameras;

    [Header("UI")]
    public GameObject[] playersUI;
    public GameObject gameOverScreen;
    public GameObject inGameScreen;
    public GameObject generalUIObject;
    public GameObject trackImagePrefab;
    public Text GameOverText;
    public TextMeshProUGUI timerText;
    public Image[] bulletBars;
    public RectTransform[] selectionImages;

    [Header("Misc")]
    public bool isSinglePlayer = false;
    public Animator[] pilotCameraAnimators;
    private bool _isGameOver = false;
    public GameObject proximitySensor;
    public ItemSpawner itemSpawner;
    public float matchTime;
    public AudioClip overtimeMarkerClip;
    public AudioClip overtimeClip;

    [Header("Colors")]
    public Color Team01Color;
    public Color Team02Color;
    public Color Team03Color;
    public Color Team04Color;

    private List<List<GameObject>> _trackImages;
    private List<ProximitySensor> _proximitySensors;
    private AudioSource _audioSource;
    private float timer;
    private bool _isOvertime;

    // Use this for initialization
    void Awake () {
        Instance = this;
        _proximitySensors = new List<ProximitySensor>();
        _trackImages = new List<List<GameObject>>();
        _audioSource = GetComponent<AudioSource>();
        itemSpawner.overTime = matchTime;
        timer = matchTime;

        //instantiate tracking images
        for ( int i = 0; i < players.Length; i++ ) {
            _trackImages.Add(new List<GameObject>());
            for ( int j = 0; j < players.Length; j++ ) {
                GameObject trackImage = Instantiate(trackImagePrefab, generalUIObject.transform);
                UITrackGameObject uitracker = trackImage.GetComponent<UITrackGameObject>();
                uitracker.renderCamera = cameras[i];
                uitracker.TrackObject = players[j];

                if ( i == j )
                    trackImage.SetActive(false);

                _trackImages[i].Add(trackImage);
                
            }

            //instantiante proximity sensors
            //GameObject ps = Instantiate(proximitySensor);
            //ProximitySensor psComponent = ps.GetComponent<ProximitySensor>();
            //psComponent.target = players[i].transform;
            //psComponent.targetTag = players[i].tag;
            //_proximitySensors.Add(psComponent);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if ( !_isOvertime ) {
            Timer();
        } else {
            if (!_audioSource.isPlaying) {
                _audioSource.clip = overtimeClip;
                _audioSource.loop = true;
                _audioSource.Play();
            }
        }
	}

    private void Timer() {
        timer -= Time.deltaTime;
        if ( timer >= 0 ) {
            string minutes = Mathf.Floor(timer / 60).ToString("00");
            string seconds = Mathf.Floor(timer % 60).ToString("00");
            timerText.text = minutes + ":" + seconds;
        } else {
            //start overtime
            inGameScreen.SetActive(false);
            _isOvertime = true;
            _audioSource.clip = overtimeMarkerClip;
            _audioSource.loop = false;
            _audioSource.Play();
        }        
    }

    public void RespawnPlayer(string playerTag, GameObject pilot) {
        switch ( playerTag ) {
            case "Player01":

                if (isSinglePlayer) {
                    for ( int i = 0; i < playersUI.Length; i++ ) {
                        playersUI[i].SetActive(false);
                    }
                }

                players[0].SetActive(false);
                camerasFollow[0].target = pilot.transform;
                pilotCameraAnimators[0].SetTrigger("camera_rotate");

                playersUI[0].SetActive(false);

                DisableTrackImage(0);
                
                StartCoroutine(SpawnPlayer(players[0], camerasFollow[0], playersUI[0], 0, respawnTime));
                break;
            case "Player02":
                players[1].SetActive(false);
                camerasFollow[1].target = pilot.transform;

                pilotCameraAnimators[1].SetTrigger("camera_rotate");

                playersUI[1].SetActive(false);

                DisableTrackImage(1);

                StartCoroutine(SpawnPlayer(players[1], camerasFollow[1], playersUI[1], 1, respawnTime));
                break;
            case "Player03":
                players[2].SetActive(false);
                camerasFollow[2].target = pilot.transform;

                pilotCameraAnimators[2].SetTrigger("camera_rotate");

                playersUI[2].SetActive(false);

                DisableTrackImage(2);

                StartCoroutine(SpawnPlayer(players[2], camerasFollow[2], playersUI[2], 2, respawnTime));
                break;
            case "Player04":
                players[3].SetActive(false);
                camerasFollow[3].target = pilot.transform;

                pilotCameraAnimators[3].SetTrigger("camera_rotate");

                playersUI[3].SetActive(false);

                DisableTrackImage(3);

                StartCoroutine(SpawnPlayer(players[3], camerasFollow[3], playersUI[3], 3, respawnTime));
                break;
            default:
                break;
        }
    }

    public void ShowGameOverUI(string playerTag) {
        if ( !_isGameOver ) {
            _isGameOver = true;
            for ( int i = 0; i < playersUI.Length; i++ ) {
                playersUI[i].SetActive(false);
            }
            for ( int i = 0; i < players.Length; i++ ) {
                players[i].SetActive(false);
                DisableTrackImage(i);
                //_proximitySensors[i].StopSensor();
            }

            if ( isSinglePlayer ) {
                switch ( playerTag ) {
                    case "Player01":
                        GameOverText.text = "Defeat!";
                        break;
                    default:
                        GameOverText.text = "Success!";
                        break;
                }
            } else {
                switch ( playerTag ) {
                    case "Player01":
                        GameOverText.text = "Player 2 Wins!";
                        break;
                    case "Player02":
                        GameOverText.text = "Player 1 Wins!";
                        break;
                    case "Player03":
                        break;
                    case "Player04":
                        break;
                    default:
                        break;
                }
            }
            gameOverScreen.SetActive(true);
        }        
    }

    IEnumerator SpawnPlayer(GameObject player, CustomCameraFollow camera, GameObject playerUI, int imageTrackId, float delayTime) {
        yield return new WaitForSeconds(delayTime);

        if(!_isGameOver ) {

            if ( isSinglePlayer ) {
                for ( int i = 0; i < playersUI.Length; i++ ) {
                    playersUI[i].SetActive(true);
                }
            }
            
            playerUI.SetActive(true);
            player.GetComponentInChildren<ObjectResetter>().Reset();
            camera.target = player.transform;
            player.SetActive(true);
            player.GetComponent<AeroplaneWeaponController>().Respawn();
            EnableTrackImage(imageTrackId);
        }        
    }

    private void DisableTrackImage(int player) {
        for ( int i = 0; i < _trackImages.Count; i++ ) {
            _trackImages[i][player].SetActive(false);
        }
    }

    private void EnableTrackImage(int player) {
        for ( int i = 0; i < _trackImages.Count; i++ ) {
            if(i != player)
                _trackImages[i][player].SetActive(true);
        }
    }

    public void UpdatePlayerCameraSpeed(string playerTag, bool isAccelerating) {
        switch ( playerTag ) {
            case "Player01":
                camerasFollow[0].Accelerate(isAccelerating);
                break;
            case "Player02":
                camerasFollow[1].Accelerate(isAccelerating);
                break;
            case "Player03":
                camerasFollow[2].Accelerate(isAccelerating);
                break;
            case "Player04":
                camerasFollow[3].Accelerate(isAccelerating);
                break;
            default:
                break;
        }

    }

    public void OnRestartButton() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnMenuButton() {
        SceneManager.LoadScene("menuGame");
    }

    public void UpdateBulletBarUI(float fillAmount, string playerTag) {
        switch ( playerTag ) {
            case "Player01":
                bulletBars[0].fillAmount = fillAmount;
                break;
            case "Player02":
                bulletBars[1].fillAmount = fillAmount;
                break;
            case "Player03":
                bulletBars[2].fillAmount = fillAmount;
                break;
            case "Player04":
                bulletBars[3].fillAmount = fillAmount;
                break;
            default:
                break;
        }
    }

    public void UpdateSelectionUI(string playerTag, Vector3 rotation) {
        switch ( playerTag ) {
            case "Player01":
                selectionImages[0].localEulerAngles = rotation;
                break;
            case "Player02":
                selectionImages[1].localEulerAngles = rotation;
                break;
            case "Player03":
                selectionImages[2].localEulerAngles = rotation;
                break;
            case "Player04":
                selectionImages[3].localEulerAngles = rotation;
                break;
            default:
                break;
        }
    }

    public void ShowBulletBarUI(bool isShowing, string playerTag) {
        switch ( playerTag ) {
            case "Player01":
                bulletBars[0].gameObject.SetActive(isShowing);
                break;
            case "Player02":
                bulletBars[1].gameObject.SetActive(isShowing);
                break;
            case "Player03":
                bulletBars[2].gameObject.SetActive(isShowing);
                break;
            case "Player04":
                bulletBars[3].gameObject.SetActive(isShowing);
                break;
            default:
                break;
        }
    }
}
